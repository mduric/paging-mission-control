package missioncontrol;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ControlProcessor {

	Map<String, List<TelemetryData>> statusMap = new HashMap<String, List<TelemetryData>>();

	private final String SEVERITY_RED_LOW = "RED LOW";
	private final String SEVERITY_RED_HIGH = "RED HIGH";

	int alertCount = 0;

	LocalDateTime timestamp;
	String satelliteID, component, severity;
	int redHighL, redLowL;
	double rawVal;

	public void processTelemetryInputLine(String data) throws CloneNotSupportedException {
		String[] tData = data.split("\\|");

		String tStamp = tData[0]; // <timestamp>
		satelliteID = tData[1]; // <satellite-id>
		String strredHighL = tData[2]; // <red-high-limit> (<yellow-high-limit> not needed)
		String strredLowL = tData[5]; // <red-low-limit> (<yellow-low-limit> not needed)
		String strrawVal = tData[6]; // <raw-value>
		component = tData[7]; // <component>

		stringToTimestamp(tStamp);
		redHighL = Integer.parseInt(strredHighL);
		redLowL = Integer.parseInt(strredLowL);
		rawVal = Double.parseDouble(strrawVal);
		TelemetryData telemetryData = new TelemetryData(timestamp, satelliteID, redHighL, redLowL, rawVal, component);

		if (!statusMap.containsKey(satelliteID)) {
			List<TelemetryData> newStatusList = new ArrayList<TelemetryData>();
			statusMap.put(satelliteID, newStatusList);
		}
		statusMap.get(satelliteID).add(telemetryData);
	}

	public void stringToTimestamp(String str) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuuMMdd HH:mm:ss.SSS");
		timestamp = LocalDateTime.parse(str, formatter);
	}

	public void iterateTelemetryData() throws CloneNotSupportedException {
		for (Map.Entry<String, List<TelemetryData>> pair : statusMap.entrySet()) { // For Loop for every Key (Satellite
																					// ID) in the Map
			List<TelemetryData> redHighLList = new ArrayList<TelemetryData>();
			List<TelemetryData> lowLimitList = new ArrayList<TelemetryData>();

			for (int i = 0; i < pair.getValue().size(); i++) { // For Loop for every List in Key
				TelemetryData tData = pair.getValue().get(i);
				timestamp = pair.getValue().get(i).getTimestamp();
				satelliteID = pair.getValue().get(i).getSatelliteId();
				redHighL = pair.getValue().get(i).getRhl();
				redLowL = pair.getValue().get(i).getRll();
				rawVal = pair.getValue().get(i).getRv();
				component = pair.getValue().get(i).getComponent();

				redHighLTest(tData, redHighLList);
				redLowLTest(tData, lowLimitList);
			}

			if (redHighLList.size() >= 3)
				redHighLAlert(redHighLList);

			if (lowLimitList.size() >= 3)
				redLowLAlert(lowLimitList);
		}
	}

	public void redHighLTest(TelemetryData sd, List<TelemetryData> highLimitLst) throws CloneNotSupportedException {
		rawVal = sd.getRv();
		redHighL = sd.getRhl();
		component = sd.getComponent();

		sd.setSeverity(SEVERITY_RED_HIGH);

		if (rawVal > redHighL && component.equals("TSTAT"))
			highLimitLst.add((TelemetryData) sd.clone());
	}

	public void redLowLTest(TelemetryData sd, List<TelemetryData> lowLimitList) throws CloneNotSupportedException {
		rawVal = sd.getRv();
		redLowL = sd.getRll();
		component = sd.getComponent();

		sd.setSeverity(SEVERITY_RED_LOW);

		if (rawVal < redLowL && component.equals("BATT"))
			lowLimitList.add((TelemetryData) sd.clone());
	}

	public void redHighLAlert(List<TelemetryData> highLimitLst) {
		for (int i = 0; i < highLimitLst.size() - 2; i++) {
			int j = i + 2;
			LocalDateTime timeA = highLimitLst.get(i).getTimestamp();
			LocalDateTime timeB = highLimitLst.get(j).getTimestamp();
			long diff = Math.abs(ChronoUnit.MINUTES.between(timeA, timeB)); // 5 Minute difference between the two times

			if (diff <= 5) { // 5 being "5 Minutes"
				satelliteID = highLimitLst.get(i).getSatelliteId();
				component = highLimitLst.get(i).getComponent();
				severity = highLimitLst.get(i).getSeverity();

				if (alertCount > 0)
					System.out.println(",");

				 System.out.println(highLimitLst.get(i));

				alertCount++;
			}
		}
	}

	public void redLowLAlert(List<TelemetryData> lowLimitList) {
		for (int i = 0; i < lowLimitList.size() - 2; i++) {
			int j = i + 2;
			LocalDateTime timeA = lowLimitList.get(i).getTimestamp();
			LocalDateTime timeB = lowLimitList.get(j).getTimestamp();
			long diff = Math.abs(ChronoUnit.MINUTES.between(timeA, timeB)); // 5 Minute difference between the two times

			if (diff <= 5) { // 5 being "5 Minutes"
				satelliteID = lowLimitList.get(i).getSatelliteId();
				component = lowLimitList.get(i).getComponent();
				severity = lowLimitList.get(i).getSeverity();

				if (alertCount > 0)
					System.out.println(",");
				
				 System.out.println(lowLimitList.get(i));

				alertCount++;
			}
		}
	}
}
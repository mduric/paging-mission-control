package missioncontrol;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class TelemetryData implements Cloneable {
	final LocalDateTime timestamp;
	final String satelliteId, component;
	final int rhl, rll;
	final double rv;
	String severity;
	
	Gson gson = new GsonBuilder().setPrettyPrinting().create();
	JsonObject jsonObject = new JsonObject();


	public TelemetryData(LocalDateTime ts, String sid, int rhl, int rll, double rv, String c) {
		this.timestamp = ts;
		this.satelliteId = sid;
		this.rhl = rhl;
		this.rll = rll;
		this.rv = rv;
		this.component = c;
	}

	LocalDateTime getTimestamp() {
		return timestamp;
	}

	String getSatelliteId() {
		return satelliteId;
	}

	int getRhl() {
		return rhl;
	}

	int getRll() {
		return rll;
	}

	double getRv() {
		return rv;
	}

	String getComponent() {
		return component;
	}

	String getSeverity() {
		return severity;
	}

	@Override
	public String toString() {
		
		jsonObject.addProperty("satelliteID", satelliteId);
		jsonObject.addProperty("severity", severity);
		jsonObject.addProperty("component", component);
		jsonObject.addProperty("timestamp", timestamp + "Z");
		
		return gson.toJson(jsonObject);
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}

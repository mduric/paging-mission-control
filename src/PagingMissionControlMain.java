package missioncontrol;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class PagingMissionControlMain {
	public static void main(String[] args) {
		BufferedReader reader;
		ControlProcessor cProcessor = new ControlProcessor();

		try {
			Scanner scanner = new Scanner(System.in);

			System.out.println("Please enter telemetry input file(full path required):");
			String filepath = scanner.nextLine();
			scanner.close();
			System.out.println("");

			reader = new BufferedReader(new FileReader(filepath));
			String line = reader.readLine();

			while (line != null) {
				cProcessor.processTelemetryInputLine(line);
				// read next line
				line = reader.readLine();
			}
			reader.close();

			System.out.println("[");
			cProcessor.iterateTelemetryData();
			System.out.println("\n]");
		} catch (Exception e) {
			System.out.println("Problem processing telemetry data.");
			e.printStackTrace();
		}
	}
}